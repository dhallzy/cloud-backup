# cloud-backup

Google cloud function to auto upload new files from google cloud bucket to s3 bucket.

Run the following command in the project directory to upload the function to gcf, replace google-cloud-storage-bucket-name with the name of your google bucket

`gcloud functions deploy backup_to_aws --runtime python37 --trigger-resource google-cloud-storage-bucket-name --trigger-event google.storage.object.finalize`