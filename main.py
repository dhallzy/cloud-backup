from google.cloud import storage
import boto3
import os


def backup_to_aws(data, context):

    # data['name'] is the path to the file just uploaded
    filepath = data['name']
    filename = os.path.basename(filepath)

    # /tmp is the only directory you can read and write in gcf
    tempPath = os.path.join('/tmp', filename)

    ######Gcloud code######
    storage_client = storage.Client()
    bucket = storage_client.get_bucket("google-cloud-storage-bucket-name")
    blob = bucket.blob(filepath)
    # download the new file to /tmp
    blob.download_to_filename(tempPath)

    #####S3 code#####
    ACCESS_KEY_ID = 'aws-access-key'
    ACCESS_SECRET_KEY = 'aws-access-secret-key'
    s3 = boto3.client('s3', aws_access_key_id=ACCESS_KEY_ID, aws_secret_access_key=ACCESS_SECRET_KEY)
    s3Bucket = 'aws-s3-bucket-name'
    s3.upload_file(tempPath, s3Bucket, filepath)

    # remove the new file from /tmp
    os.remove(tempPath)
